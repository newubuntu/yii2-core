<?php

namespace emilasp\core;

use emilasp\core\components\OptionsData;
use emilasp\core\extensions\colorbox\Colorbox;
use emilasp\themes\Themes;

class FrontCoreModule extends CoreModule
{
    //public $controllerNamespace = 'emilasp\core\controllers';

    public $enableThemes = true;
    public $modalSkin = Colorbox::SKIN_1;
    public $bootSkin = Themes::THEME_TATUHA;

    /**
     * @var OptionsData
     */
    public $h_options;

    public $theme = 'tattoo';

    public function init()
    {
        parent::init();


        // custom initialization code goes here
    }


}
