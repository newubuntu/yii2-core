<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 11.10.14
 * Time: 2:17
 */

namespace emilasp\core\behaviors;

use emilasp\core\helpers\EStringHelper;
use yii;
use yii\base\Behavior;

class AvtorBehavior extends Behavior
{

    public $attribute = 'avtor_id';

    public function events()
    {
        return [
            yii\db\ActiveRecord::EVENT_BEFORE_INSERT => 'setAvtorId',
            yii\db\ActiveRecord::EVENT_BEFORE_UPDATE => 'setAvtorId',
            /* yii\db\ActiveRecord::EVENT_AFTER_INSERT => 'saveImage',
            yii\db\ActiveRecord::EVENT_AFTER_UPDATE => 'saveImage',*/
        ];
    }

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
        //
    }


    public function setAvtorId( $title=false, $fullUrl = false )
    {
        if ( $this->owner->isNewRecord || is_null($this->owner->{$this->attribute}) ) {

            $this->owner->{$this->attribute} = Yii::$app->user->identity->id;
        }
    }

    public function isOwner(){

        $isOwner = false;
        if( !Yii::$app->user->isGuest && $this->owner->{$this->attribute}==Yii::$app->user->identity->id ){
            $isOwner = true;
        }

        return $isOwner;
    }


}
