<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 11.10.14
 * Time: 2:17
 */

namespace emilasp\core\behaviors;

use yii;
use yii\base\Behavior;

class DateBehavior extends Behavior
{

    public $moduleId;

    public function events()
    {
        return [
           /* yii\db\ActiveRecord::EVENT_BEFORE_INSERT => 'returnImageName',
            yii\db\ActiveRecord::EVENT_BEFORE_UPDATE => 'returnImageName',
            yii\db\ActiveRecord::EVENT_AFTER_INSERT => 'saveImage',
            yii\db\ActiveRecord::EVENT_AFTER_UPDATE => 'saveImage',*/
        ];
    }

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
        //
    }


    public function getDate( $attribute = 'datecreate' )
    {
        return \emilasp\core\helpers\EFormatterHelper::getPupleDate($this->owner->{$attribute});
    }

    public function getDatetime( $attribute = 'datecreate' )
    {
        return \emilasp\core\helpers\EFormatterHelper::getPupleDatetime($this->owner->{$attribute});
    }



}
