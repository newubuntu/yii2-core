<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 11.10.14
 * Time: 2:17
 */

namespace emilasp\core\behaviors;

use emilasp\core\helpers\EImageHelper;
use emilasp\core\helpers\ESiteHelper;
use emilasp\core\helpers\EStringHelper;
use yii;
use yii\base\Behavior;

class ImageBehavior extends Behavior
{
    const TYPE_FROM_URL = 1;
    const TYPE_FROM_PATH = 2;
    const TYPE_FROM_PATH_SERVER = 3;

    public $attribute;

    public $typeSave = self::TYPE_FROM_PATH;

    public function events()
    {
        return [
            yii\db\ActiveRecord::EVENT_BEFORE_INSERT => 'returnImageName',
            yii\db\ActiveRecord::EVENT_BEFORE_UPDATE => 'returnImageName',
            yii\db\ActiveRecord::EVENT_AFTER_INSERT => 'saveImage',
            yii\db\ActiveRecord::EVENT_AFTER_UPDATE => 'saveImage',
        ];
    }

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
        //
    }

    public function returnImageName(){
        if(isset($this->owner->oldAttributes[$this->attribute]))
            $this->owner->{$this->attribute} = $this->owner->oldAttributes[$this->attribute];
    }

    public function saveImage(){

        if($this->typeSave==self::TYPE_FROM_PATH)
            EImageHelper::saveModelImage($this->owner,$this->attribute);
        elseif($this->typeSave==self::TYPE_FROM_PATH_SERVER)
            EImageHelper::saveModelImage($this->owner,$this->attribute, false);
        else
            EImageHelper::saveModelImageFromUrl($this->owner,$this->attribute);
    }

    /**
     * Получаем ссылку на изображение
     * @param string $type размер
     * @param bool $fullUrl полный url
     * @return bool|string
     */
    public function getImage( $type = 'min', $fullUrl = false){

        $modelName = EStringHelper::getClassName($this->owner, true, 'front');
        $path = \Yii::getAlias( \Yii::$app->params['media']['path'].'/'.$modelName.'/'.$this->owner->id.'/'.$type.'_'.$this->owner->{$this->attribute} );
        $url = ESiteHelper::pathToUrl($path);

        if(is_file($path)){
            return yii\helpers\Url::to($url, $fullUrl);
        }

        if(!isset(\Yii::$app->params['media']['types'][$modelName])) $modelName = 'default';

        $defaultImage = \Yii::getAlias(\Yii::$app->params['media']['defaultImageFolder']).'/'.$modelName.'/'.\Yii::$app->params['media']['types'][$modelName]['sizes'][$type]['default'];
        if(isset($defaultImage) ){
            return ESiteHelper::pathToUrl($defaultImage);
        }
        return false;
    }


}
