<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 11.10.14
 * Time: 2:17
 */

namespace emilasp\core\behaviors;

use emilasp\core\helpers\EStringHelper;
use yii;
use yii\base\Behavior;

class ModelUrlBehavior extends Behavior
{

    public $moduleId;
    public $attrName = 'name';

    public function events()
    {
        return [
           /* yii\db\ActiveRecord::EVENT_BEFORE_INSERT => 'returnImageName',
            yii\db\ActiveRecord::EVENT_BEFORE_UPDATE => 'returnImageName',
            yii\db\ActiveRecord::EVENT_AFTER_INSERT => 'saveImage',
            yii\db\ActiveRecord::EVENT_AFTER_UPDATE => 'saveImage',*/
        ];
    }

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
        //
    }

    /**
     *  Получаем url для страницы модели
     * @param bool $title
     * @param bool $fullUrl - полный Url
     * @param bool}string $action
     * @return string
     */
    public function getUrl( $title=false, $fullUrl = false, $action = false )
    {
        if(!$action) $action = 'view';
        $route = '/'.$this->moduleId.'/'.EStringHelper::getClassName($this->owner,true,'front').'/'.$action.'/';
        if($title!==false) return yii\helpers\Url::toRoute([$route,'id'=>$this->owner->id,'title'=>$title]);

        return yii\helpers\Url::toRoute([$route,'id'=>$this->owner->id,'title'=>EStringHelper::str2url($this->owner->{$this->attrName})], $fullUrl);
    }

    /**
     *  Проверяем текущий url на валидность и делаем редирект или выкидываем исключение
     * @param string|bool $url
     * @param bool $except
     * @param bool|string $action
     * @return bool|string
     * @throws \yii\web\NotFoundHttpException
     */

    public function checkUrl($url = false, $except=false, $action = false){

        $urlModel = $this->owner->getUrl(false,false,$action);

        if(!$url){
            $url = Yii::$app->request->getUrl(true);
            if(strpos($url,'.html?')) {
                $arrUrl = explode('.html?',$url);
                $url = $arrUrl[0].'.html';
            }
        }

        if($urlModel==$url){
            return $url;
        }

        if($except){
            throw new yii\web\NotFoundHttpException('Указанный url не существует');
        }else{
            Yii::$app->controller->redirect(yii\helpers\Url::to($urlModel), 301);
        }

        return false;
    }

}
