<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 11.10.14
 * Time: 2:17
 */

namespace emilasp\core\behaviors;

use yii;
use yii\base\Behavior;

class Search extends Behavior
{
    public $attribute = 'searched';


    public function events()
    {
        return [
            yii\db\ActiveRecord::EVENT_INIT => 'addSearched'
        ];
    }

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        /*if (empty($this->attributes)) {
            $this->attributes = [
                yii\db\BaseActiveRecord::EVENT_BEFORE_INSERT => [$this->createdAtAttribute, $this->updatedAtAttribute],
                yii\db\BaseActiveRecord::EVENT_BEFORE_UPDATE => $this->updatedAtAttribute,
            ];
        }*/
    }

    public function addSearched(){
        $this->owner->searched = true;
    }
}
