<?php

namespace emilasp\core\components;

use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Controller;

/**
 * Site controller
 */
class CController extends  Controller
{
    public function actions()
    {
        return [
           /* 'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
            ],*/
        ];
    }

    public function beforeAction($action)
    {
        if (parent::beforeAction($action)) {
            if( Yii::$app->controller->action->id!="login" && Yii::$app->user->isGuest)
                Yii::$app->getResponse()->redirect('/user/default/login');
            return true;
        } else {
            return false;
        }
    }

    //TODO before save dateupdate
    //TODO before delete if isset attribute status -> status = 0 else delete model
}
