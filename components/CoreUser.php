<?php

namespace emilasp\core\components;

use Yii;
use yii\base\Component;
use yii\helpers\Json;
use yii\web\User;

/**
 * Компонент для отправки почты
 *
 * Class Accounts
 * @package emilasp\core\components
 */
class CoreUser extends User
{

    const PREFIX_PREV_URLS = 'user_urls';

    public $prefix = 'user_data';

    public function init()
    {
        parent::init();
    }

    /**
     * Сохраняем данные в сесссии
     *
     * @param string $prefix
     * @param array $data
     */
    public function setData($prefix, $data){

        if(is_array($data)){
            $_data = [];
            foreach($data as $index=>$_value){
                $_data[$index] = $_value;
            }
            if( count($_data>0))
                \Yii::$app->session->set($prefix, $_data);
        }

    }

    /**
     * Получаем данные из сессии
     * @param string $prefix
     * @param null $name
     * @param null $defaultValue
     * @return mixed|null
     */
    public function getData($prefix, $name = null, $defaultValue = null){

        $data = \Yii::$app->session->get($prefix, []);

        if(is_null($name)) return $data;

        if(isset($data[$name])) return $data[$name];

        return $defaultValue;
    }

    /**
     * Сохраняем предыдущие url
     * @param $url Текущий Url
     */
    public function setPrevUrls( $url ){

        $this->setReturnUrl($url);

        $prev = Json::encode([
                'url'=>$url,
                'params'=>Yii::$app->request->getQueryParams(),
                'action'=>Yii::$app->requestedAction->id,
                'controller'=>Yii::$app->requestedAction->controller->id,
                'module'=>Yii::$app->requestedAction->controller->module->id,
            ]);

        $preprev = $this->getData(self::PREFIX_PREV_URLS, 'prev',null);
        $this->setData(self::PREFIX_PREV_URLS,[
                'preprev'=>$preprev,
                'prev'=>$prev,
            ]);
    }

    /**
     * Получаем предыдущие Url
     * @param null|String $type
     * @return array|String
     */
    public function getPrevUrls( $type = null ){
        $urls = [];
        $urls['prev'] = Json::decode($this->getData(self::PREFIX_PREV_URLS, 'prev',null));
        $urls['preprev'] = Json::decode($this->getData(self::PREFIX_PREV_URLS, 'preprev',null));
        if(is_null($type)||$type!='prev'||$type!='preprev')
            return $urls;
        return $urls[$type];
    }

}
