<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 14.08.14
 * Time: 17:43
 */

namespace emilasp\core\extensions\colorbox;



use emilasp\core\components\OptionsData;
use yii;


class Colorbox  extends \yii\base\Widget {

    const SKIN_1 = '1';
    const SKIN_2 = '2';
    const SKIN_3 = '3';
    const SKIN_4 = '4';
    const SKIN_5 = '5';

    public static $icons = [
        OptionsData::MESSAGE_SUCCESS => 'heart',
        OptionsData::MESSAGE_INFO => 'star-empty',
        OptionsData::MESSAGE_ERROR => 'remove-sign',
        OptionsData::MESSAGE_USER => 'user',
    ];

    public function init(){
        $this->registerAssets();
    }

    public function run(){
        //echo $this->render('colorbox');
    }

    /**
     * Register client assets
     */
    public function registerAssets()
    {
        $view = $this->getView();
        ColorboxAsset::register($view);

        //
        $js =
<<<JS
$('a.gallery').colorbox({rel:'gallery', transition:"fade", slideshow:true});
JS;
        $view->registerJs($js,yii\web\View::POS_READY);
    }

}
