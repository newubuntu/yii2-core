<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 14.08.14
 * Time: 17:43
 */

namespace emilasp\core\extensions\eListView;

use yii;


class EListView  extends yii\widgets\ListView {

    public $layout = "<div class='clearfix'>{summary}\n{sorter}</div>\n{items}\n{pager}";
    public $pager = [];


    public function init(){
        parent::init();

        $this->pager = [
            'firstPageLabel'    => Yii::t('site','First'),
            'lastPageLabel'     => Yii::t('site','Last'),
            'nextPageLabel'     => '>',
            'prevPageLabel'     => '<',
        ];

        $this->registerAssets();
    }

    public function run(){
        parent::run();
        //echo $this->render('colorbox');
    }

    /**
     * Register client assets
     */
    public function registerAssets()
    {
        $view = $this->getView();
        EListViewAsset::register($view);
        //
        $js =
<<<JS
JS;
        $view->registerJs($js,yii\web\View::POS_LOAD);
    }

}
