<?php
namespace emilasp\core\extensions\eListView;


use kartik\widgets\AssetBundle;

class EListViewAsset extends AssetBundle
{

    public $depends = [
        'yii\web\YiiAsset',
        'yii\web\JqueryAsset'
    ];

    public function init()
    {
        $this->setSourcePath(__DIR__ . '/assets');

        $this->setupAssets('css', [
                'listview',
            ]);

        $this->setupAssets('js', [
                'listview',
            ]);

        //$this->setupAssets('css', ['alert','admin']);
        parent::init();
    }



}
