<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 14.08.14
 * Time: 17:43
 */

namespace emilasp\core\extensions\jbox;

use yii;
use  yii\helpers\Html;


class JBox  extends \yii\base\Widget {

    const TYPE_MODAL = 'Modal';
    const TYPE_TOOLTIP = 'Tooltip';
    const TYPE_NOTICE = 'Notice';

    public $type;
    public $idButton = 'modal';
    public $idContent = false;

    public $clientOptions = [];
    private $jsonOptions;



    public function init(){
        $this->generateOptions();
        $this->registerAssets();
        $this->beginWidget();
    }

    public function run(){
        $this->endWidget();
        //echo $this->render('alert');
    }

    private function generateOptions(){
        if($this->idContent)
            $this->clientOptions['content'] = "#replaceContent#";


        $this->jsonOptions = yii\helpers\Json::encode($this->clientOptions);
        $this->jsonOptions = str_replace('"#replaceContent#"',"$('#{$this->idContent}')",$this->jsonOptions);
        $this->jsonOptions = str_replace('"onOpen":"','"onOpen":',$this->jsonOptions);
        $this->jsonOptions = str_replace('"onClose":"','"onClose":',$this->jsonOptions);
        $this->jsonOptions = str_replace(';}"','; }',$this->jsonOptions);
    }

    /**
     * Register client assets
     */
    private function registerAssets()
    {
        $view = $this->getView();
        JBoxAsset::register($view);
        $js =
            <<<JS

        //$( document ).ready(function() {
            var modal{$this->idContent} = $('#{$this->idButton}, .{$this->idButton}').jBox('{$this->type}', {$this->jsonOptions});
            //modal{$this->idContent}.setTitle( $().data('title') );
        //});

JS;
        $view->registerJs($js,yii\web\View::POS_READY);
    }

    private function beginWidget(){
        echo Html::beginTag('div',[
                'id'=>$this->idContent,
                'style'=>'display: none',
            ]);
    }

    private function endWidget(){
        echo Html::endTag('div');
    }

}
