<?php

namespace emilasp\core\extensions\jbox;
use kartik\widgets\AssetBundle;
use yii\web\View;

class JBoxAsset extends AssetBundle
{

    public $jsOptions=['position'=>View::POS_HEAD];

    //public $sourcePath = 'emilasp/account/extensions/AdminSkin/assets/';

    public $depends = [
        'yii\web\YiiAsset',
        'yii\web\JqueryAsset'
    ];

    public function init()
    {
        $this->setSourcePath(__DIR__ . '/assets');

        $this->setupAssets('css', [
                'jBox-0.3.1/Source/jBox',
                'jBox-0.3.1/Source/themes/NoticeBorder',
                'jBox-0.3.1/Source/themes/TooltipBorder',
                'jbox',
            ]);

        $this->setupAssets('js', [
                'jBox-0.3.1/Source/jBox.min',
                'tooltips',
            ]);

        //$this->setupAssets('css', ['alert','admin']);
        parent::init();
    }


}
