<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 14.08.14
 * Time: 17:43
 */

namespace emilasp\core\extensions\jbox;

use yii;

class JBoxGallery  extends JBox {



    public function init(){
        $this->registerAssets();
        parent::init();
    }

    public function run(){
        //
        parent::run();
    }

    /**
     * Register client assets
     */
    public function registerAssets()
    {
        $view = $this->getView();

        $js =
<<<JS
    new jBox('Image',{imageSize:"cover"});
JS;
        $view->registerJs($js,yii\web\View::POS_READY);
    }
}
