<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 14.08.14
 * Time: 17:43
 */

namespace emilasp\core\extensions\jbox;

use yii;

class JBoxModal  extends JBox {

    public $type = self::TYPE_MODAL;
    //public $idButton = 'modal';
    //public $idContent = false;

    public $clientOptions = [];

    public $title = '';

    private $_clientOptions = [
        'animation' => 'flip',
        'addClass' => 'jbox-modal',
        //'theme' => 'BorderBlue'
    ];



    public function init(){
        $this->_clientOptions['title'] = $this->title;

        $this->clientOptions = yii\helpers\ArrayHelper::merge($this->_clientOptions,$this->clientOptions);

        parent::init();
    }

    public function run(){
        //
        parent::run();
    }


}
