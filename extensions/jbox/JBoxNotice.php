<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 14.08.14
 * Time: 17:43
 */

namespace emilasp\core\extensions\jbox;

use yii;

class JBoxNotice  extends yii\base\Widget {

    const TYPE_ERROR = 'error';
    const TYPE_SUCCESS = 'success';
    const TYPE_INFO = 'info';
    const TYPE_USER = 'user';

    public $position = [
        'x'=>'right',
        'y'=>'top',
    ];

    public $sound = true;

    public $delayOpen  = false;
    public $delayClose = 0;


    public $type;

    private $typeBox = JBox::TYPE_NOTICE;

    public $clientOptions = [];

    public $title = '';
    public $content = '';

    private $_clientOptions = [
        'addClass' => 'jbox-notice',
       // 'theme' => 'NoticeBorder',
        //'theme' => 'BorderBlue'
    ];
    private $jsonOptions;

    private $urlAsset;

    public function init(){

        $this->registerAsset();

        $this->_clientOptions['title'] = $this->title;
        $this->_clientOptions['content'] = $this->content;
        $this->_clientOptions['position'] = $this->position;
        //$this->_clientOptions['delayOpen'] = $this->delayOpen;
        //$this->_clientOptions['delayClose'] = $this->delayClose;

        $this->setOptionsToType();

        $this->clientOptions = yii\helpers\ArrayHelper::merge($this->_clientOptions,$this->clientOptions);


        $this->generateOptions();

        $this->registerJs();
    }

    public function run(){

    }

    private function generateOptions(){
        $this->jsonOptions = yii\helpers\Json::encode($this->clientOptions);
    }

    private function registerAsset(){
        JBoxAsset::register($this->getView());
    }
    /**
     * Register client assets
     */
    private function registerJs()
    {

        $scriptJbox =


        $scriptJbox =
            <<<JS
        new jBox('{$this->typeBox}', {$this->jsonOptions});
JS;

        $js =
            <<<JS
         setTimeout(function(){        {$scriptJbox}} , {$this->delayOpen});
JS;

if(!$this->delayOpen) $js = $scriptJbox;


        $this->getView()->registerJs($js,yii\web\View::POS_READY);
    }

    private function setOptionsToType(){

        //if($this->sound)
            //$this->_clientOptions['audio'] = 'http://yii2.tatuha.ru'.\Yii::$app->assetManager->bundles['emilasp\core\extensions\jbox\JBoxAsset']->baseUrl.'/jBox-0.3.1/Source/audio/bling2';

        switch($this->type){
            case self::TYPE_ERROR:
                $this->_clientOptions['theme'] = 'NoticeBorder';
                $this->_clientOptions['color'] = 'red';
                //if($this->sound)
                    //$this->_clientOptions['audio'] = 'http://yii2.tatuha.ru'.\Yii::$app->assetManager->bundles['emilasp\core\extensions\jbox']->baseUrl.'/jBox-0.3.1/Source/audio/boop1';
                $this->_clientOptions['volume'] = '100';
                //$this->_clientOptions['stack'] = 'true';
                $this->_clientOptions['animation'] = 'tada';
                break;
            case self::TYPE_SUCCESS:
                $this->_clientOptions['color'] = 'green';
                //$this->_clientOptions['stack'] = 'true';
                $this->_clientOptions['animation'] = 'flip';
                break;
            case self::TYPE_INFO:
                $this->_clientOptions['color'] = 'blue';
                //$this->_clientOptions['stack'] = 'true';
                $this->_clientOptions['animation'] = 'flip';
                break;
            case self::TYPE_USER:
                $this->_clientOptions['color'] = 'yellow';
                //$this->_clientOptions['stack'] = 'true';
                $this->_clientOptions['animation'] = 'flip';
                break;
            default:
                $this->_clientOptions['color'] = 'blue';
                //$this->_clientOptions['stack'] = 'true';
                $this->_clientOptions['animation'] = 'flip';
                break;
        }
    }

}
