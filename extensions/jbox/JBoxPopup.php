<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 14.08.14
 * Time: 17:43
 */

namespace emilasp\core\extensions\jbox;

use yii;

class JBoxPopup  extends JBox {

    public $type = self::TYPE_TOOLTIP;
    //public $idButton = 'modal';
    //public $idContent = false;

    public $clientOptions = [];

    public $title = '';

    private $_clientOptions = [
        'addClass' => 'jbox-popup',
        'animation' => 'flip',
        'closeOnMouseleave' => 'true',
        'delayOpen' => '200',
        'delayClose' => '200',
        'position'=>[
            'x'=>'center',
            'y'=>'bottom',
        ],
        //'adjustPosition'=>'move',
        /*'adjustDistance'=>[
            'top'=>'500',
            'right'=>'200',
            'bottom'=>'50',
            'left'=>'200',
        ],*/
        'outside'=>'y',
        'adjustPosition'=>'true',
        'adjustTracker'=>'true',
        'pointer'=>'center:75',
        'width'=>'300',
        'theme'=>'TooltipBorder',
        //'theme' => 'BorderBlue'
        /*'onOpen'=>'function() {
                        this.setContent("jBox is opening…");
                    }',
        'onClose'=>'function() {
                    this.setContent("jBox is closing…");
                }',*/
    ];



    public function init(){
        $this->_clientOptions['title'] = $this->title;

        $this->clientOptions = yii\helpers\ArrayHelper::merge($this->_clientOptions,$this->clientOptions);

        parent::init();
    }

    public function run(){
        //
        parent::run();
    }
/*$('.tooltip').jBox('Tooltip', {
content: 'You can move your mouse here<br>and interact with this tooltip',
closeOnMouseleave: true
});*/

}
