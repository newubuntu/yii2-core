widgets Modal and Tooltip on jBox для Yii2
=============================

Данное расширение добавляет возможность подключения, добавления и парсинга shortcode.

Installation
------------

The preferred way to install this extension is through [composer](http://getcomposer.org/download/).

Add to composer.json:
```json
"emilasp/yii2-shortcode": "*"
```
AND
```json
"repositories": [
        {
            "type": "git",
            "url":  "https://bitbucket.org/emilasp/yii2-shortcode.git"
        }
    ]
```

to the require section of your `composer.json` file.

Configuring
Modal:
--------------------------
<div id="myModal">Click me to open a modal window!</div>
<?php \emilasp\widgets\jbox\JBoxModal::begin([
        'title'=>'Title',
        'idContent'=>'newModal',
        'idButton'=>'openNewModal',
    ]); ?>


Какойто текст
<?php \emilasp\widgets\jbox\JBoxModal::end(); ?>


Tooltip:
--------------------------
<div class="tooltip-click" title="Tooltip">Какойто текст</div>
<div class="tooltip-hover" title="Tooltip">Какойто текст</div>
<div class="tooltip-hover-lt" title="Tooltip">Какойто текст</div>
<div class="tooltip-hover-rt" title="Tooltip">Какойто текст</div>
<div class="tooltip-hover-lb" title="Tooltip">Какойто текст</div>
<div class="tooltip-hover-rb" title="Tooltip">Какойто текст</div>

Alerts:
--------------------------
 <?php \emilasp\widgets\jbox\JBoxNotice::widget([
            'type'=>$type,
            'delayOpen'=>$index*600,
            //'title'=>\emilasp\core\components\OptionsData::$messages[$type],
            'content'=>$text
        ]);


Menu PopUp:
--------------------------
<?php \emilasp\widgets\jbox\JBoxPopup::begin([
        'title'=>'Title',
        'idContent'=>'newModal',
        'idButton'=>'openNewModal',
        'animation' => 'flip',
        'closeOnMouseleave' => 'true',
        'delayOpen' => '200',
        'delayClose' => '200',
        'position'=>[
            'x'=>'center',
            'y'=>'bottom',
        ],
        'outside'=>'y',
        'adjustPosition'=>'true',
        'adjustTracker'=>'true',
        'pointer'=>'center:75',
        'width'=>'300',
        'theme'=>'NoticeBorder',
    ]); ?>


Какойто текст
<?php \emilasp\widgets\jbox\JBoxPopup::end(); ?>