<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 14.08.14
 * Time: 17:43
 */

namespace emilasp\core\extensions\jsHelpers;



use emilasp\core\components\OptionsData;
use emilasp\core\extensions\colorbox\Colorbox;
use yii;


class JsHelpers  extends \yii\base\Widget {



    public static $icons = [
        OptionsData::MESSAGE_SUCCESS => 'heart',
        OptionsData::MESSAGE_INFO => 'star-empty',
        OptionsData::MESSAGE_ERROR => 'remove-sign',
        OptionsData::MESSAGE_USER => 'user',
    ];

    public function init(){
        $this->registerAssets();
        echo Colorbox::widget();
    }

    public function run(){
        //echo $this->render('jsHelpers');
    }

    /**
     * Register client assets
     */
    public function registerAssets()
    {
        $view = $this->getView();
        JsHelpersAsset::register($view);
    }

}
