<?php

namespace emilasp\core\extensions\jsHelpers;
use kartik\widgets\AssetBundle;
use rmrevin\yii\minify\View;

class JsHelpersAsset extends AssetBundle
{

    public $jsOptions=['position'=>View::POS_HEAD];

    //public $sourcePath = 'emilasp/account/extensions/AdminSkin/assets/';

    public $depends = [
        'yii\web\YiiAsset',
        'yii\web\JqueryAsset'
    ];

    public function init()
    {
        $this->setSourcePath(__DIR__ . '/assets');
        $this->setupAssets('js', ['alert','load','scrollTo']);
        $this->setupAssets('css', ['css']);
        parent::init();
    }


}
