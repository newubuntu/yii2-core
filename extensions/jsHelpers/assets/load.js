$.fn.loading = function(show){
    var obj = this;
    if(show){
        obj.prepend('<div class="loading-centered"></div>');
        obj.children('.loading-centered').show('fast');
        console.log('show loading');
    }else{
        $('.loading-centered').hide('fast',function(){obj.children('.loading-centered').remove()});
    }
};