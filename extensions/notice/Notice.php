<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 14.08.14
 * Time: 17:43
 */

namespace emilasp\core\extensions\notice;



use emilasp\core\components\OptionsData;
use yii;


class Notice  extends \yii\base\Widget {



    public static $icons = [
        OptionsData::MESSAGE_SUCCESS => 'heart',
        OptionsData::MESSAGE_INFO => 'star-empty',
        OptionsData::MESSAGE_ERROR => 'remove-sign',
        OptionsData::MESSAGE_USER => 'user',
    ];

    public function init(){
        //$this->registerAssets();
    }

    public function run(){
        echo $this->render('notices');
    }

    /**
     * Register client assets
     */
    public function registerAssets()
    {
        $view = $this->getView();
        NoticeAsset::register($view);
    }

}
