<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 29.09.14
 * Time: 21:24
 */

namespace emilasp\core\helpers;


use yii\helpers\StringHelper;

class EArrayHelper {

    /**
     * Генерируем уникальное имя для файлов в частности
     *
     * @param bool $ext
     * @return string
     */
    public static  function generateName( $ext = false ){

        $name = time().(($ext)?'.'.$ext:'');

        return $name;
    }

    /**
     * Удаляем все элементы равные $param из массива
     *
     * @param array $arr
     * @param string $param
     * @return array
     */
    public static function deleteArrayElements($arr, $param = ''){
        return array_diff($arr,array($param));
    }

} 