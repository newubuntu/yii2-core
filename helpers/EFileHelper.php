<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 29.09.14
 * Time: 21:24
 */

namespace emilasp\core\helpers;


use yii\base\Exception;
use yii\base\InvalidParamException;
use yii\helpers\FileHelper;
use yii\helpers\StringHelper;
use yii\log\Logger;

class EFileHelper extends FileHelper {


    public static  function findFolders( $dir, $count=false ){

        if (!is_dir($dir)) {
            throw new InvalidParamException('The dir argument must be a directory.');
        }

        $list = [];
        $handle = opendir($dir);
        if ($handle === false) {
            throw new InvalidParamException('Unable to open directory: ' . $dir);
        }
        while (($file = readdir($handle)) !== false) {
            if ($file === '.' || $file === '..') {
                continue;
            }
            $path = $dir . DIRECTORY_SEPARATOR . $file;
            if (!is_file($path)) {
                if($count) $list[$file] = static::findFiles($path);
                else $list[] = $file;
            }
        }
        closedir($handle);
        return $list;
    }


    /**
     * Получаем массив с данными по файлам в папке
     *
     * @param $path
     * @return array
     */
    public static function getListFileInfo( $path ){

        $arrReturn = [];

        $dh = opendir($path);
        while($filename = readdir($dh))
        {

            $fs = filesize($path.$filename);
            $ft = filetype($path.$filename);
            $fd = filemtime($path.$filename);

            //echo "Имя: ".$filename."\nРазмер: ".$fs."\nТип:".$ft;

            if( $ft=='file' AND strpos($filename, '.tar.gz') )
                $arrReturn[] = [
                    'name'=>$filename,
                    'size'=>$fs,
                    'date'=>date('d-m-Y H:i:s' ,$fd),
                    'type'=>$ft,
                ];
        }

        return $arrReturn;
    }

    public static function createDirectory($path, $mode = 0755, $recursive=true){
        $path = \Yii::getAlias($path);
        parent::createDirectory($path, $mode, $recursive);
    }


    /*public static function saveImage($pathImg, $newPathImg, $width, $height, $clean=false, $ratio=false, $convert=false, $newNameBody = false, $watermark=false, $noSetCompanent = false){

        if(!$noSetCompanent)
            Yii::app()->setComponents(array('imagemod'=>array('class'=>'application.extensions.imagemodifier.CImageModifier')));

        $img = Yii::app()->imagemod->load($pathImg);
        $img->image_resize          = true;
        if($img->image_src_x<$width) $img->image_resize = false;
        if($ratio){
            switch ($ratio) {
                case 'y':		$img->image_ratio_y	= true; break;
                case 'x':		$img->image_ratio_x	= true; break;
                case 'crop':	$img->image_ratio_crop = true; break;
                case 'fill':	$img->image_ratio_fill = true; break;
                default: 		$img->image_ratio_y = true;
            };
        }
        if($convert) $img->image_convert = $convert;
        if($watermark) $img->image_watermark = $watermark;
        if($newNameBody) $img->file_new_name_body = $newNameBody;

        if($width>400 && $watermark)
            $img->image_watermark = Yii::app()->params['waterMark'];
        $img->image_watermark_position = 'BR';


        $img->jpeg_quality = 100;
        $img->image_y = $height;
        $img->image_x = $width;
        $img->process($newPathImg);

        if ($img->processed) {
            if($clean) $img->clean();
            return $img->file_src_name;
        } else {
            echo $img->error.' '.$newPathImg;
            Yii::app()->end();
            ActiveRecord::dump_file($img->error);
            return false;
        }
    }

    public static function saveImagesFromInternet($folder,$url,$dopFolder=false){


        if($url=="") return "";
        //$folder = 'gImageHelperallery/'.Yii::app()->user->id.'/files';

        if($dopFolder){
            $pathDopFolder = Yii::app()->params['media'].'/'.$dopFolder;
            if(!file_exists($pathDopFolder))
                mkdir($pathDopFolder, 0777);
        }
        $pathFolder = Yii::app()->params['media'].'/'.$folder;
        if(!file_exists($pathFolder))
            mkdir($pathFolder, 0777);

        $path = Yii::app()->params['media'].'/'.$folder.'/';
        $newNameBody = time();

        $info = self::getImageInfo($url);

        $imgNameFUrl = $path.$newNameBody.'.'.$info['ext'];

        copy($url, $imgNameFUrl);
        //copy($url, $path.$newNameBody.'.jpg');



        $pathImg = $imgNameFUrl;

        self::createAllImageSize($pathImg,$path,$newNameBody);

        return $newNameBody.'.jpg';
    }

    public static function getImageInfo($pathToImage){
        $img_params =@getimagesize($pathToImage);

        if(!is_array($img_params)) return false;
        $img_params['ext'] = 'jpg';
        switch ($img_params[2])
        {
            case '1':
                $img_params['ext'] = 'gif';
                break;

            case '3':
                $img_params['ext'] = 'png';
                break;

            case '2':

                $img_params['ext'] = 'jpg';
                break;
        }
        return $img_params;
    }



    **
     * @param ActiveRecord $model - Модель для которой сохраняем изображение
     * @param string $attribute - атрибут у модели для получения из формы
     * @param bool|string $folder - папка
     * @param bool $del_sorce - удалить оригинальное изображение
     * @param bool $beforeDelPath - перед созданием изображений очистить папку
     * @param bool $addId - добавить в путь id моделиs\
     * @return string
     *

    public static function saveImages($model, $attribute, $folder=false, $del_sorce=false, $beforeDelPath=false, $addId=false){

        if(!$folder) $folder = strtolower(get_class($model));

        $path = Yii::app()->params['media'].'/'.$folder.'/';

        if($addId) $path .= $model->id.'/';

        $newNameBody = time();

        $image = CUploadedFile::getInstance($model,$attribute);
        $name_img = self::image_name($image);

        if(!empty($image)){

            if($beforeDelPath)
                self::rmDirRec(realpath($path));

            if(!file_exists($path))
                mkdir($path, 0777);

            if(!$image->saveAs($path.$name_img)){
                //print_r($model->getErrors());
                Yii::log(var_dump($model->getErrors()),CLogger::LEVEL_ERROR);
                return '';
            }else{
                $pathImg = $path.$name_img;
                self::createAllImageSize($pathImg,$path,$newNameBody);
            }
        } else return '';

        return $newNameBody.'.jpg';
    }

    public static function rmDirRec($dir)
    {
        $objs = glob($dir."/*");
        if ($objs)
        {
            foreach($objs as $obj)
            {
                $tt = is_dir($obj) ? "" : @unlink($obj);
            }
        }
        @rmdir($dir);
    }

    public static function image_name($name){
        $arrN = explode(".", $name);
        $name = time().'.'.end($arrN);
        return strtolower($name);
    }


    public static function createAllImageSize($pathImg,$path,$newNameBody){
        self::saveImage($pathImg,$path,50,40,false,'crop','jpg','ico_'.$newNameBody,false,false);
        self::saveImage($pathImg,$path,88,70,false,'crop','jpg','min_'.$newNameBody,false,true);
        self::saveImage($pathImg,$path,150,120,false,'crop','jpg','mid_'.$newNameBody,false,true);
        self::saveImage($pathImg,$path,450,100,false,'y','jpg','med_'.$newNameBody,true,true);
        self::saveImage($pathImg,$path,800,100,false,'y','jpg','big_'.$newNameBody,true,true);
        self::saveImage($pathImg,$path,1024,100,true,'y','jpg','max_'.$newNameBody,true,true);//$del_sorce
    }

    public static function getUnicNameFile($title, $path, $i=false){

        if($i) {
            $title_e = $title.'_'.$i;
        }else {
            $i = 0;
            $title_e = $title;
        }


        if(is_file($path.'ico_'.$title_e.'.jpg')){
            $i++;
            $title_e = self::getUnicNameFile($title, $path, $i);
        }
        return $title_e;
    }


    public static function newNameBodyFileFromTitle($title, $path=false){


        $title = str_replace(')','', $title);
        $title = str_replace(' -','-', $title);
        $title = str_replace('- ','-', $title);
        $title = str_replace('-','_', $title);
        $array = explode(' ', $title);

        if(count($array)>1) $title = $array[0]."_".$array[1];
        elseif(count($array)==1) $title = $array[0];
        else $title = time();

        $str = EStringHelper::str2url($title);
        $str = str_replace('-','_', $str);

        if($path)
            $str = self::getUnicNameFile($str, $path);

        return $str;

    }

    public static function listFolder($folder, $typeReturn="folder",$withFilesCount=false){
        $dir = opendir($folder);
        $arrFiles = array();
        $arrFolders = array();
        while ($file_name = readdir($dir)) {
            //clearstatcache();
            //echo '---> : ',$file_name;
            if($file_name=="."||$file_name=="..") continue;

            $val = $file_name;
            if($withFilesCount) {
                if (is_dir($folder.'/'.$file_name)){
                    $val = $file_name.'('.count(self::listFolder($folder.'/'.$file_name,'files')).'';

                    if (is_dir($folder.'/'.$file_name.'/min/'))
                        $val .= '/'.count(self::listFolder($folder.'/'.$file_name.'/min','files'));
                    $val .= ')';
                }


            }

            if (is_dir($folder.'/'.$file_name)) $arrFolders[$file_name] = $val;
            if (is_file($folder.'/'.$file_name)) $arrFiles[$file_name] = $val;
            //echo ''.dirname($file_name).' *** '.basename($file_name);
        }
        closedir($dir);
        $return = array();
        switch ($typeReturn){
            case "folder":
                $return = $arrFolders;
                break;
            case "files":
                $return = $arrFiles;
                break;
            case "all":
                $return = array_merge($arrFolders,$arrFiles);
                break;
        }

        return $return;
    }

    public static function addMinImgToFolder($folder){

        $minSize = 500;

        $pathMin = $folder.'min/';
        $pathMax = $folder.'max/';

        if(!file_exists($pathMin))
            mkdir($pathMin, 0777);
        if(!file_exists($pathMax))
            mkdir($pathMax, 0777);

        $arrFiles = self::listFolder($folder,"files");

        $minSizeC = 0;
        $allImgC = count($arrFiles);
        $addImgC = 0;
        $errorNameC = 0;
        $errorCreateC = 0;

        foreach($arrFiles as $key=>$filename){

            //echo '('.$filename.')'.PHP_EOL;
            if(file_exists($folder.$filename)){

                if(strpos($filename,'<')||strpos($filename,'>')) {
                    unlink($folder.$filename);
                    $errorNameC++;
                    continue;
                }

                try{
                    $size = getimagesize ($folder.$filename);
                    if($size[0]<$minSize AND $size[1]<$minSize) {
                        $minSizeC++;
                        unlink($folder.$filename);
                        continue;
                    }
                    if(!file_exists($pathMax.$filename))
                        self::saveImage($folder.$filename,$pathMax,1024,100,false,'y','jpg',$filename,false,false);

                    if(!file_exists($pathMin.$filename))
                        self::saveImage($folder.$filename,$pathMin,75,75,false,'crop','jpg',$filename,false,true);

                    //if (copy($folder.$filename,"/usr/local/src/code.c")) {
                    //    unlink($folder.$filename);
                    //}

                    unlink($folder.$filename);
                    $addImgC++;
                }catch (Exception $e){
                    $errorCreateC++;
                    echo 'Не удалось сгенерировать изображения ('.$filename.')'.PHP_EOL;
                }
            }
        }
        echo 'Всего: '.$allImgC.'('.($allImgC-$minSizeC-$errorNameC-$errorCreateC).')'.PHP_EOL;
        echo 'Добавлено: '.$addImgC.PHP_EOL;
        echo 'Маленьких: '.$minSizeC.PHP_EOL;
        echo 'Ошибка имени: '.$errorNameC.PHP_EOL;
        echo 'Ошибка создания: '.$errorCreateC.PHP_EOL;
    }*/

}