<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 29.09.14
 * Time: 21:24
 */

namespace emilasp\core\helpers;


use Imagine\Image\Box;
use Imagine\Image\BoxInterface;
use Imagine\Image\ImageInterface;
use Imagine\Image\ManipulatorInterface;
use Imagine\Image\Point;
use yii\base\Exception;
use yii\imagine\Image;
use yii\web\UploadedFile;

/**
 * Класс хелпер для работы с изобраежниями.
 *
 * 1) Все изображения для моделей сохраняются в папке media/className/model->id/image.jpg
 *
 *
 * Class EImageHelper
 * @package emilasp\core\helpers
 */
class EImageHelper extends Image {


    /**
     * Сохраняем изображение для модели + сохраняем имя новго изображения в модели.
     * @param $model
     * @param string $attribute
     * @param bool $uploaded
     * @return bool|string
     */
    public static function saveModelImage($model, $attribute, $uploaded = true){

        if( $uploaded ){
            $fileObj = UploadedFile::getInstance($model, $attribute);
            if(is_null($fileObj)) return false;
            $file = $fileObj->tempName;
        }else{
            $file = $model->{$attribute};
        }

        return self::saveImageToModel($model, $attribute, $file);
    }

    /**
     * Сохраняем изображение из интернета для модели
     * @param object $model
     * @param string $attribute
     * @return array|bool|mixed
     */
    public static function saveModelImageFromUrl($model, $attribute){

        $file = self::saveImagesFromInternet($model->$attribute);

        return self::saveImageToModel($model, $attribute, $file);
    }


    public static function saveImageToModel( $model, $attribute, $file ){
        if(is_null($file)) return false;

        $name = EStringHelper::str2url($model->name);

        $_name = self::saveAllImages($file, EStringHelper::getClassName($model, true, ['front','parsers']), $model->id, false, $name);

        if($_name){
            \Yii::$app->db->createCommand()->update($model->tableName(), [$attribute => $_name], 'id=:id',[':id'=>$model->id])->execute();
        }
        return $_name;
    }

    //$name = emilasp\core\helpers\EImageHelper::saveAllImages($imageUpload, 'posts', \Yii::$app->getUser()->id);

    /** Сохраняем все размеры изображений
     * @param $image
     * @param string $modelClass
     * @param string $dopFolder
     * @param bool $returnParam
     * @param bool|string $name
     * @return array|bool|mixed
     */
    public static function saveAllImages($image, $modelClass = 'site', $dopFolder = '', $returnParam = false, $name = true ){

        //$params =  ( isset(\Yii::$app->modules['media'])?\Yii::$app->modules['media']: \Yii::$app->modules->media );//\Yii::$app->params;
        $params = \Yii::$app->params['media'];//\Yii::$app->params;

        $pathToSave = \Yii::getAlias($params['path'].'/'.$modelClass.'/'.$dopFolder);

        EFileHelper::createDirectory($pathToSave, 0777, true);

        $mediaType = (isset($params['types'][$modelClass]))?$params['types'][$modelClass]:$params['types']['default'];

        $sizes = $mediaType['sizes'];

        $count = 1;
        foreach(  $sizes as $type=>$media ){

            if($count < count($sizes))
                $delete = false;
            else
                $delete = true;

            $crop = ($media['type']=='crop')?true:false;

            $_name = self::saveImage( $image, $pathToSave, $media['size']['width'], $media['size']['height'], $name, $type, $mediaType['watermark'], $delete, $crop );
            $count++;
        }

        if($returnParam) return ['path'=>$pathToSave, 'name'=>$_name];
        return $_name;
    }


    /**
     * Метод сохраняет имеющееся изображение в новом формате/размере по указанному пути
     *
     * @param string $image Путь до изображения
     * @param string $pathToSave Путь для сохранения
     * @param integer $width
     * @param integer $height
     * @param bool|string $newName - новое имя true - сгенерировать, false как есть в пути, string - задать
     * @param bool|string $preName - префикс имени
     * @param bool|string $watermark - если нужен водный знак, то строка с путём до изображения водного знака иначе false
     * @param bool $delete - удалить старый файл после сохранения
     * @param bool $crop - пропорции crop, ratio
     * @return bool|string
     */
    public static function saveImage( $image, $pathToSave, $width, $height, $newName = true, $preName = true, $watermark = true, $delete = true, $crop = false ){

        //$params = ArrayHelper::toArray( \Yii::$app->modules['media'] );//\Yii::$app->params;
        $params = \Yii::$app->params['media'];

        if( $newName===true )
            $newName = EStringHelper::generateName( $params['defaultExtForImage'] );
        else
            $newName = $newName.'.'.$params['defaultExtForImage'];

        if( $preName ) $_newName = $preName.'_'.$newName; else $_newName = $newName;

        try{

            if($crop)
                $thumb = self::getImageCrop($image, $width, $height, $mode = ManipulatorInterface::THUMBNAIL_OUTBOUND);
            else
                $thumb = self::getImageRatio($image, $width, $height, $mode = ManipulatorInterface::THUMBNAIL_OUTBOUND);

            if($watermark && $thumb->getSize()->getWidth()>200 ){
                $watermark =  Image::getImagine()->open(\Yii::getAlias($params['waterMark']));
                $scale = $params['scaleWaterMark'];
                $watermark = self::getImageRatio($watermark, $thumb->getSize()->getWidth()/$scale, $thumb->getSize()->getHeight()/$scale) ;
                $thumb->paste($watermark, new Point(10, 10));
            }

            $thumb->save(\Yii::getAlias($pathToSave.'/'.$_newName), ['quality' => $params['quality']]);

            if($delete ){
                unlink(\Yii::getAlias($image));
            }

        }catch ( Exception $e){
            \Yii::getLogger()->log('Error save Image: '.var_dump($e), Logger::LEVEL_ERROR, 'binary');
            return false;
        }

        return $newName;
    }

    /**
     * Метод возвращает отресайженное по максимальному ратио и откропленое по размеру изображение в виде объекта ImageInterface
     *
     * @param $filename string | ImageInterface
     * @param $width integer
     * @param $height integer
     * @return ImageInterface
     */
    public static function getImageCrop($filename, $width, $height){

        if(  is_object($filename) )
            $img = $filename;
        else{
            $imagine = Image::getImagine();
            $img = $imagine->open(\Yii::getAlias($filename));
        }

        $size = $img->getSize();
        $ratio = self::inset(new Box($width, $height), $size->getWidth(), $size->getHeight(), true);
        $thumbnailSize = $size->scale($ratio);

        $img->resize($thumbnailSize);

        $_width = ($img->getSize()->getWidth()+$width)/2-$img->getSize()->getWidth();
        $_height = ($img->getSize()->getHeight()+$height)/2-$img->getSize()->getHeight();

        $img->crop( new Point(abs($_width),abs($_height)), new Box($width, $height));
        return $img;
        //->save($destination);
    }

    /**
     * Метод возвращает отресайженое изображение в виде объекта ImageInterface
     *
     * @param $filename string | ImageInterface
     * @param $width integer
     * @param $height integer
     * @return ImageInterface
     */
    public static function getImageRatio($filename, $width, $height){

        if(  is_object($filename) )
            $img = $filename;
        else{
            $imagine = Image::getImagine();
            $img = $imagine->open(\Yii::getAlias($filename));
        }

        $size = $img->getSize();
        $ratio = self::inset(new Box($width, $height), $size->getWidth(), $size->getHeight());
        $thumbnailSize = $size->scale($ratio);

        $img->resize($thumbnailSize);

        return $img;
        //->save($destination);
    }

    /**
     * Считаем ратио - ресайзим изображение так что бы оно влазило в прямоугольник, но сохраняло при этом пропорции.
     *
     * @param BoxInterface $size
     * @param integer $_width
     * @param integer $_height
     * @param bool $outset
     * @return mixed
     */
    public static  function inset(BoxInterface $size, $_width, $_height, $outset = false)
    {
        $width  = $size->getWidth();
        $height = $size->getHeight();

        if($outset){
            $ratio = max(array(
                $width / $_width,
                $height / $_height
            ));
        }else{
            $ratio = min(array(
                $width / $_width,
                $height / $_height
            ));
        }

        return $ratio;
    }

    /**
     * Сохраняем изображение из интернета
     *
     * @param string $url
     * @param string|bool $path
     * @return null|string Путь до сохранённого изображения
     */
    public static function saveImagesFromInternet($url, $path = false){

        if( $url=="" || (strpos($url,'http://')===false && strpos($url,'https://')===false)) return null;

        if(!$path) $pathTmp = \Yii::$app->params['media']['path'].'/tmp';
        else       $pathTmp = \Yii::getAlias($path);

        EFileHelper::createDirectory($pathTmp, 0777, true);

        $newNameBody = time().rand(1,100);
        $info = self::getImageInfo($url);
        $imgNameFUrl = $pathTmp.'/'.$newNameBody.'.'.$info['ext'];
        copy($url, \Yii::getAlias($imgNameFUrl));

        return $imgNameFUrl;
    }


    /**
     * Получаем информацию о файле, включая расширение файла
     * @param string $pathToImage
     * @return array|bool
     */
    public static function getImageInfo($pathToImage){
        $img_params =@getimagesize($pathToImage);

        if(!is_array($img_params)) return false;
        $img_params['ext'] = 'jpg';
        switch ($img_params[2])
        {
            case '1':
                $img_params['ext'] = 'gif';
                break;
            case '3':
                $img_params['ext'] = 'png';
                break;
            case '2':
                $img_params['ext'] = 'jpg';
                break;
        }
        return $img_params;
    }



    public static function getDefaultImage($type = 'tattoo', $size = 'min'){
        return \emilasp\core\helpers\ESiteHelper::pathToUrl(\Yii::getAlias(\Yii::$app->params['media']['defaultImageFolder']).'/'.$type.'/'.\Yii::$app->params['media']['types'][$type]['sizes'][$size]['default']);
    }














    /*public static function saveImage($pathImg, $newPathImg, $width, $height, $clean=false, $ratio=false, $convert=false, $newNameBody = false, $watermark=false, $noSetCompanent = false){

        if(!$noSetCompanent)
            Yii::app()->setComponents(array('imagemod'=>array('class'=>'application.extensions.imagemodifier.CImageModifier')));

        $img = Yii::app()->imagemod->load($pathImg);
        $img->image_resize          = true;
        if($img->image_src_x<$width) $img->image_resize = false;
        if($ratio){
            switch ($ratio) {
                case 'y':		$img->image_ratio_y	= true; break;
                case 'x':		$img->image_ratio_x	= true; break;
                case 'crop':	$img->image_ratio_crop = true; break;
                case 'fill':	$img->image_ratio_fill = true; break;
                default: 		$img->image_ratio_y = true;
            };
        }
        if($convert) $img->image_convert = $convert;
        if($watermark) $img->image_watermark = $watermark;
        if($newNameBody) $img->file_new_name_body = $newNameBody;

        if($width>400 && $watermark)
            $img->image_watermark = Yii::app()->params['waterMark'];
        $img->image_watermark_position = 'BR';


        $img->jpeg_quality = 100;
        $img->image_y = $height;
        $img->image_x = $width;
        $img->process($newPathImg);

        if ($img->processed) {
            if($clean) $img->clean();
            return $img->file_src_name;
        } else {
            echo $img->error.' '.$newPathImg;
            Yii::app()->end();
            ActiveRecord::dump_file($img->error);
            return false;
        }
    }

    public static function saveImagesFromInternet($folder,$url,$dopFolder=false){


        if($url=="") return "";
        //$folder = 'gallery/'.Yii::app()->user->id.'/files';

        if($dopFolder){
            $pathDopFolder = Yii::app()->params['media'].'/'.$dopFolder;
            if(!file_exists($pathDopFolder))
                mkdir($pathDopFolder, 0777);
        }
        $pathFolder = Yii::app()->params['media'].'/'.$folder;
        if(!file_exists($pathFolder))
            mkdir($pathFolder, 0777);

        $path = Yii::app()->params['media'].'/'.$folder.'/';
        $newNameBody = time();

        $info = self::getImageInfo($url);

        $imgNameFUrl = $path.$newNameBody.'.'.$info['ext'];

        copy($url, $imgNameFUrl);
        //copy($url, $path.$newNameBody.'.jpg');



        $pathImg = $imgNameFUrl;

        self::createAllImageSize($pathImg,$path,$newNameBody);

        return $newNameBody.'.jpg';
    }





    **
     * @param ActiveRecord $model - Модель для которой сохраняем изображение
     * @param string $attribute - атрибут у модели для получения из формы
     * @param bool|string $folder - папка
     * @param bool $del_sorce - удалить оригинальное изображение
     * @param bool $beforeDelPath - перед созданием изображений очистить папку
     * @param bool $addId - добавить в путь id моделиs\
     * @return string
     *

    public static function saveImages($model, $attribute, $folder=false, $del_sorce=false, $beforeDelPath=false, $addId=false){

        if(!$folder) $folder = strtolower(get_class($model));

        $path = Yii::app()->params['media'].'/'.$folder.'/';

        if($addId) $path .= $model->id.'/';

        $newNameBody = time();

        $image = CUploadedFile::getInstance($model,$attribute);
        $name_img = self::image_name($image);

        if(!empty($image)){

            if($beforeDelPath)
                self::rmDirRec(realpath($path));

            if(!file_exists($path))
                mkdir($path, 0777);

            if(!$image->saveAs($path.$name_img)){
                //print_r($model->getErrors());
                Yii::log(var_dump($model->getErrors()),CLogger::LEVEL_ERROR);
                return '';
            }else{
                $pathImg = $path.$name_img;
                self::createAllImageSize($pathImg,$path,$newNameBody);
            }
        } else return '';

        return $newNameBody.'.jpg';
    }

    public static function rmDirRec($dir)
    {
        $objs = glob($dir."/*");
        if ($objs)
        {
            foreach($objs as $obj)
            {
                $tt = is_dir($obj) ? "" : @unlink($obj);
            }
        }
        @rmdir($dir);
    }

    public static function image_name($name){
        $arrN = explode(".", $name);
        $name = time().'.'.end($arrN);
        return strtolower($name);
    }


    public static function createAllImageSize($pathImg,$path,$newNameBody){
        self::saveImage($pathImg,$path,50,40,false,'crop','jpg','ico_'.$newNameBody,false,false);
        self::saveImage($pathImg,$path,88,70,false,'crop','jpg','min_'.$newNameBody,false,true);
        self::saveImage($pathImg,$path,150,120,false,'crop','jpg','mid_'.$newNameBody,false,true);
        self::saveImage($pathImg,$path,450,100,false,'y','jpg','med_'.$newNameBody,true,true);
        self::saveImage($pathImg,$path,800,100,false,'y','jpg','big_'.$newNameBody,true,true);
        self::saveImage($pathImg,$path,1024,100,true,'y','jpg','max_'.$newNameBody,true,true);//$del_sorce
    }

    public static function getUnicNameFile($title, $path, $i=false){

        if($i) {
            $title_e = $title.'_'.$i;
        }else {
            $i = 0;
            $title_e = $title;
        }


        if(is_file($path.'ico_'.$title_e.'.jpg')){
            $i++;
            $title_e = self::getUnicNameFile($title, $path, $i);
        }
        return $title_e;
    }


    public static function newNameBodyFileFromTitle($title, $path=false){


        $title = str_replace(')','', $title);
        $title = str_replace(' -','-', $title);
        $title = str_replace('- ','-', $title);
        $title = str_replace('-','_', $title);
        $array = explode(' ', $title);

        if(count($array)>1) $title = $array[0]."_".$array[1];
        elseif(count($array)==1) $title = $array[0];
        else $title = time();

        $str = EStringHelper::str2url($title);
        $str = str_replace('-','_', $str);

        if($path)
            $str = self::getUnicNameFile($str, $path);

        return $str;

    }

    public static function listFolder($folder, $typeReturn="folder",$withFilesCount=false){
        $dir = opendir($folder);
        $arrFiles = array();
        $arrFolders = array();
        while ($file_name = readdir($dir)) {
            //clearstatcache();
            //echo '---> : ',$file_name;
            if($file_name=="."||$file_name=="..") continue;

            $val = $file_name;
            if($withFilesCount) {
                if (is_dir($folder.'/'.$file_name)){
                    $val = $file_name.'('.count(self::listFolder($folder.'/'.$file_name,'files')).'';

                    if (is_dir($folder.'/'.$file_name.'/min/'))
                        $val .= '/'.count(self::listFolder($folder.'/'.$file_name.'/min','files'));
                    $val .= ')';
                }


            }

            if (is_dir($folder.'/'.$file_name)) $arrFolders[$file_name] = $val;
            if (is_file($folder.'/'.$file_name)) $arrFiles[$file_name] = $val;
            //echo ''.dirname($file_name).' *** '.basename($file_name);
        }
        closedir($dir);
        $return = array();
        switch ($typeReturn){
            case "folder":
                $return = $arrFolders;
                break;
            case "files":
                $return = $arrFiles;
                break;
            case "all":
                $return = array_merge($arrFolders,$arrFiles);
                break;
        }

        return $return;
    }

    public static function addMinImgToFolder($folder){

        $minSize = 500;

        $pathMin = $folder.'min/';
        $pathMax = $folder.'max/';

        if(!file_exists($pathMin))
            mkdir($pathMin, 0777);
        if(!file_exists($pathMax))
            mkdir($pathMax, 0777);

        $arrFiles = self::listFolder($folder,"files");

        $minSizeC = 0;
        $allImgC = count($arrFiles);
        $addImgC = 0;
        $errorNameC = 0;
        $errorCreateC = 0;

        foreach($arrFiles as $key=>$filename){

            //echo '('.$filename.')'.PHP_EOL;
            if(file_exists($folder.$filename)){

                if(strpos($filename,'<')||strpos($filename,'>')) {
                    unlink($folder.$filename);
                    $errorNameC++;
                    continue;
                }

                try{
                    $size = getimagesize ($folder.$filename);
                    if($size[0]<$minSize AND $size[1]<$minSize) {
                        $minSizeC++;
                        unlink($folder.$filename);
                        continue;
                    }
                    if(!file_exists($pathMax.$filename))
                        self::saveImage($folder.$filename,$pathMax,1024,100,false,'y','jpg',$filename,false,false);

                    if(!file_exists($pathMin.$filename))
                        self::saveImage($folder.$filename,$pathMin,75,75,false,'crop','jpg',$filename,false,true);

                    //if (copy($folder.$filename,"/usr/local/src/code.c")) {
                    //    unlink($folder.$filename);
                    //}

                    unlink($folder.$filename);
                    $addImgC++;
                }catch (Exception $e){
                    $errorCreateC++;
                    echo 'Не удалось сгенерировать изображения ('.$filename.')'.PHP_EOL;
                }
            }
        }
        echo 'Всего: '.$allImgC.'('.($allImgC-$minSizeC-$errorNameC-$errorCreateC).')'.PHP_EOL;
        echo 'Добавлено: '.$addImgC.PHP_EOL;
        echo 'Маленьких: '.$minSizeC.PHP_EOL;
        echo 'Ошибка имени: '.$errorNameC.PHP_EOL;
        echo 'Ошибка создания: '.$errorCreateC.PHP_EOL;
    }*/

}