<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 29.09.14
 * Time: 21:24
 */

namespace emilasp\core\helpers;


use yii\helpers\StringHelper;

class ESiteHelper {

    /**
     * @param bool $scheme
     * @return string
     */
    public static function getDomein( $scheme = true){
        return (($scheme)?'http://':'').trim( \Yii::$app->request->getServerName() ,',' );
    }

    /**
     * @param $url
     * @param bool $scheme
     * @return string
     */
    public static function getDomeinFromUrl( $url, $scheme=true ){
        $parse =  parse_url($url);
        return (($scheme)?$parse['scheme'].'://':'').$parse['host'];
    }

    /**
     * @param $path
     * @return mixed
     */
    public static function pathToUrl( $path ) {
        return str_replace( \Yii::getAlias('@app/web/'), '/', $path);
    }
} 