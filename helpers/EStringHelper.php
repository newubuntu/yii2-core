<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 29.09.14
 * Time: 21:24
 */

namespace emilasp\core\helpers;


use yii\helpers\Html;
use yii\helpers\StringHelper;

class EStringHelper {



    /**
     * @param $text
     * @param string $delimiter
     * @return array
     */
    public static function stringParts( $text, $delimiter = "\n"){
        $parts =  explode($delimiter,$text);
        return $parts;
    }



    /**
     * Генерируем уникальное имя для файлов в частности
     *
     * @param bool|string $ext
     * @param bool|string $title
     * @param integer $words
     * @return string
     */
    public static  function generateName( $ext = false, $title = false, $words = 5 ){

        if( $title ){
            $name = self::str2url($title);

            $arrWords = explode( '-', $title );

            $_name = '';
            for($i=1; $i<=$words; $i++){
                if(!isset($arrWords[$i])) break;
                $_name .= '-'.$arrWords[$i];
            }

            if(strlen($_name)>5) $name = $_name;

        }else
            $name = time();

        $name = $name.(($ext)?'.'.$ext:'');

        return $name;
    }

    public static function rus2translit($string) {
        $converter = array(
            'а' => 'a',   'б' => 'b',   'в' => 'v',
            'г' => 'g',   'д' => 'd',   'е' => 'e',
            'ё' => 'e',   'ж' => 'zh',  'з' => 'z',
            'и' => 'i',   'й' => 'y',   'к' => 'k',
            'л' => 'l',   'м' => 'm',   'н' => 'n',
            'о' => 'o',   'п' => 'p',   'р' => 'r',
            'с' => 's',   'т' => 't',   'у' => 'u',
            'ф' => 'f',   'х' => 'h',   'ц' => 'c',
            'ч' => 'ch',  'ш' => 'sh',  'щ' => 'sch',
            'ь' => '',  'ы' => 'y',   'ъ' => '',
            'э' => 'e',   'ю' => 'yu',  'я' => 'ya',

            'А' => 'A',   'Б' => 'B',   'В' => 'V',
            'Г' => 'G',   'Д' => 'D',   'Е' => 'E',
            'Ё' => 'E',   'Ж' => 'Zh',  'З' => 'Z',
            'И' => 'I',   'Й' => 'Y',   'К' => 'K',
            'Л' => 'L',   'М' => 'M',   'Н' => 'N',
            'О' => 'O',   'П' => 'P',   'Р' => 'R',
            'С' => 'S',   'Т' => 'T',   'У' => 'U',
            'Ф' => 'F',   'Х' => 'H',   'Ц' => 'C',
            'Ч' => 'Ch',  'Ш' => 'Sh',  'Щ' => 'Sch',
            'Ь' => '',  'Ы' => 'Y',   'Ъ' => '',
            'Э' => 'E',   'Ю' => 'Yu',  'Я' => 'Ya',
        );
        return strtr($string, $converter);
    }

    public static function str2url($str) {
        // переводим в транслит
        $str = self::rus2translit($str);
        // в нижний регистр
        $str = strtolower($str);
        // заменям все ненужное нам на "-"
        $str = preg_replace('~[^-a-z0-9_]+~u', '-', $str);
        $str = preg_replace('/([_-])\\1+/', '$1', $str);
        // удаляем начальные и конечные '-'
        $str = trim($str, "-");
        return $str;
    }

    public static function timeLast($time){
        if ($time > 86400)  {
            return gmdate("d д.", $time-86400) . gmdate(" h ч. i м.", $time);
        }else {
            return gmdate("h ч. i м.", $time);
        }
    }

    public static function strToArrayEval($string){
        /*
         * "folder"=>"eeee",
         * "file"=>""
         * "folder"=>"eeee","file"=>""
         *
         */

        $returnArray = array();

        $string = str_replace(array("\r\n", "\r", "\n"),',',$string);
        $string = str_replace('"','',$string);
        $string = str_replace(',,',',',$string);
        $string = str_replace("'",'',$string);

        $arr1 = explode(',',$string);

        foreach($arr1 as $ar1){
            $arr2 = explode('=>',$ar1);
            if($arr2[0]!="")
                $returnArray[trim($arr2[0])] = trim($arr2[1]);
        }
        return $returnArray;

    }


    public static function getClass( $model ){
        return $model::className();
    }

    /**
     * Получаем имя класса объекта
     * @param object $model
     * @param bool|string|array $delString
     * @param bool $strToLower
     * @return string
     */
    public static function getClassName( $model, $strToLower = true, $delString = false ){

        if(is_string($model)){
            $className = StringHelper::basename($model);
        }else{
            $className = StringHelper::basename(get_class($model));
        }

        if($strToLower) $className = strtolower($className);
        if($delString)  {
            if(is_array($delString)){
                foreach( $delString as $str ){
                    $className = str_replace($str,'',$className);
                }
            }else
                $className = str_replace($delString,'',$className);
        }
        return $className;
    }


    /**
     * Обрезаем строку до нужно длины по словам
     * @param string $str
     * @param int $maxLen
     * @param string $simbol
     * @param string $afterText
     * @return string
     */
    public static function truncateString( $str, $maxLen = 500, $simbol = ' ', $afterText = '' )
    {
        if ( mb_strlen( $str ) > $maxLen ) {
            preg_match( '/^.{0,'.$maxLen.'}'.$simbol.'.*?/ui', $str, $match );
            return $match[0].$afterText;
        }
        return $str;
    }


    /**
     * Удаляем определённые символы/слова из строки
     * @param string $string
     * @param string $symbols
     * @return mixed
     */
    public static function deleteSymbols( $string ,$symbols = '\r\n\t' ){
        return  preg_replace('/['.$symbols.']+(?![^(]*\))/', "", $string);
    }

    /**
     * Очищаем и обрезаем строку для Мета тегов страницы
     * @param string $description
     * @param int $length
     * @return mixed|string
     */
    public static function toMeta( $description, $length = 1000 ){
        $description =  htmlspecialchars(strip_tags(html_entity_decode($description)));
        //$description = preg_replace('%[^A-Za-zА-Яа-я0-9.-]%', '', $description);
        $description = self::deleteSymbols($description);
        $description = self::truncateString($description,$length);

        return trim($description);
    }


    public static  function firstParagraf($string, $delimiter = '</p>', $minLength = 500){


        $arr = explode($delimiter, $string);

        $arrNew = [];
        $len = 0;
        foreach( $arr as $paragraph){
            $arrNew[] = $paragraph;
            $len += strlen($paragraph);
            if($len>$minLength) break;
        }

        if(count($arrNew)>0) {
             return implode($delimiter, $arrNew).$delimiter;
        }

        return $string;
    }

} 